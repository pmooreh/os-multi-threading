#include "pic.h"
#include "stdint.h"
#include "machine.h"

#define C1 0x20           /* command port for PIC1 */
#define D1 (C1 + 1)       /* data port for PIC1 */
#define C2 0xA0           /* command port for PIC2 */
#define D2 (C2 + 1)       /* data port for PIC2 */

uint32_t picBase = 32;

void picInit(void) {
    /* ICW1 */
    outb(C1,0x11);        /* init with ICW4, not single */
    outb(C2,0x11);        /* init with ICW4, not single */

    /* ICW2 */
    outb(D1,picBase);    /* IDT index for IRQ0 */
    outb(D2,picBase+8);  /* IDT index for IRQ8 */

    /* ICW3 */
    outb(D1, 1 << 2);     /* tells master that the save is at IRQ2 */
    outb(D2, 2);          /* tells salve that it's connected at IRQ2 */

    /* ICW4 */
    outb(D1, 1);          /* 8086 mode */
    outb(D2, 1);          /* 8086 mode */

    /* enable all */
    outb(D1,0);
    outb(D2,0);
}

void picEnable() {
    sti();
}
    
int picDisable() {
    return (cli() >> 9) & 1;
}

void picRestore(int wasEnabled) {
    if (wasEnabled) {
        picEnable();
    } else {
        picDisable();
    }
}
    

void picEoi(uint32_t irq) {
    if (irq >= 8) {
        /* let PIC2 know */
        outb(C2,0x20);
    }
    /* we always let PIC1 know because PIC2 is routed though PIC1 */
    outb(C1,0x20);
}
