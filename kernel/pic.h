#ifndef _PIC_H_
#define _PIC_H_

#include "stdint.h"

extern uint32_t picBase;

extern void picInit(void);
extern void picEoi(uint32_t irq);
extern void picEnable(void);
extern int picDisable(void);
extern void picRestore(int wasEnabled);

#endif
