#ifndef _THREAD_H_
#define _THREAD_H_

extern void threadInit(void);

typedef void (*ThreadFunc)(void*);

extern int threadCreate(ThreadFunc func, void* arg);
extern void threadYield(void);
extern void threadExit(void);
extern int threadId(void);

#endif
