
	# swapContext(int oldState, void* newStackPointer, void** oldStackPointer)
	# pushes old register values onto the stack, saves oldStackPointer
	# loads newStackPOinter, pop new register values off stack
	.global swapContext
swapContext:
	# push all callee saved registers
	push %ebx
	push %esi
	push %edi
	push %ebp

	# and all other registers except %esp
	push %eax
	push %edx
	push %ecx

	# and eflags, i guess
#	push %eflags
#	pushf


    # mov oldStackPointer into %ecx
    mov 36(%esp), %ecx

    # mov new stack pointer into %edx
    mov 32(%esp), %edx





	# save current stack location to oldStackPointer
	mov %esp, (%ecx)

	# load new stack pointer as THE stack pointer
	mov %edx, %esp
	
	# pop eflags
#	pop %eflags
#	popf

	# pop off all the register values
	pop %ecx
	pop %edx
	pop %eax

	pop %ebp
	pop %edi
	pop %esi
	pop %ebx

	call picEnable

	# we're done here. return!
	ret	


# void contextSwitch(long newESP, long* saveOldESP)

	.global contextSwitch
contextSwitch:
	push %eax
	push %ecx
	mov 12(%esp),%eax     # newESP
	mov 16(%esp),%ecx     # *saveOldESP
	push %edx
	push %ebx
	push %esi
	push %edi
	push %ebp

	mov %esp,(%ecx)
	mov %eax,%esp

	.global resume
resume:
	pop %ebp
	pop %edi
	pop %esi
	pop %ebx
	pop %edx
	pop %ecx
	pop %eax

	ret

# void contextResult(esp)

	.global contextResume
contextResume:

	mov 4(%esp),%esp # new ESP
	jmp resume
