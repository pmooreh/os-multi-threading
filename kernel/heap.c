#include "heap.h"
#include "console.h"
#include "stdint.h"
#include "pic.h"



int* heap;
int heapLength;

int pos(int num) {
    if (num < 0)
        return -num;
    return num;
}

int nextMultipleOfFour(int x) {
	int r = x % 4;
	if (r != 0)
		return x + (4 - r);
	return x;
}

void heapInit(void* base, size_t bytes) {
    int oldState = picDisable();

    // heap arr starts at base
    heap = (int*)base;
    heapLength = bytes / 4;

    // put end blocks
    heap[0] = -4;
    heap[heapLength - 1] = -4;

    // put first free blocks
    heap[1] = 4 * (heapLength - 2);
    heap[heapLength - 2] = 4 * (heapLength - 2);
    // done!

    picRestore(oldState);
}

void* malloc(size_t bytes) {
    int oldState = picDisable();

    int sizeRequired = nextMultipleOfFour(bytes + 8);

    // worst fit. find the largest free block
    // int largestBlockIndex = 0;
    // for (int i = 0; i < heapLength;) {
    //     int blockLength = heap[i];

    //     if (blockLength > heap[largestBlockIndex]) {
    //         largestBlockIndex = i;
    //     }
    //     i = i + (pos(blockLength) / 4);

    // }

    // first fit. needed for added speed!!
    int blockIndex = 0;
    int blockLength = heap[blockIndex];
    while (blockLength < sizeRequired && blockIndex < heapLength) {
        blockIndex = blockIndex + (pos(blockLength) / 4);
        blockLength = heap[blockIndex];
    }

    int sizeOfLargest = heap[blockIndex];
    int lengthOfLargest = sizeOfLargest / 4;

    int newSizeOfLargest = sizeOfLargest - sizeRequired;
    int newLengthOfLargest = newSizeOfLargest / 4;
    
    // if the resultant block is < 16 bytes, give whole block
    if (sizeOfLargest < sizeRequired) {
        picRestore(oldState);
        return 0;
    }
    else if (newSizeOfLargest < 16) {
        heap[blockIndex] = -sizeOfLargest;
        heap[blockIndex + lengthOfLargest - 1] = -sizeOfLargest;

        picRestore(oldState);
        // return (void*)(((blockIndex + 1) * 4) + (int)&heap[0]);
        return (void*)&heap[blockIndex + 1];
    }
    else {
        // put newly allocated block at the end of the old block (largest)
        heap[blockIndex] = newSizeOfLargest;
        heap[blockIndex + newLengthOfLargest - 1] = newSizeOfLargest;
        heap[blockIndex + newLengthOfLargest] = -sizeRequired;
        heap[blockIndex + lengthOfLargest - 1] = -sizeRequired;

        picRestore(oldState);
        // return (void *)(((blockIndex + newLengthOfLargest + 1) * 4) + (int)&heap[0]);
        return (void*)&heap[blockIndex + newLengthOfLargest + 1];
    }
}        

void free(void* p) {
    int oldState = picDisable();
	// simple free: just reset the header and footers to show positive size
	int index = (((int)p - (int)&heap[0]) / 4) - 1;

	int size = -heap[index];
	int length = size / 4;

	int sizeLeft = heap[index - 1];
	int lengthLeft = sizeLeft / 4;

	int sizeRight = heap[index + length];
	int lengthRight = sizeRight / 4;

	if (sizeLeft < 0 && sizeRight < 0) {
		// no merge
		heap[index] = size;
		heap[index + length - 1] = size;
	}

    if (sizeLeft > 0 && sizeRight < 0) {
		// merge left only
		int totalSize = sizeLeft + size;
		heap[index - lengthLeft] = totalSize;
		heap[index + length - 1] = totalSize;
    }

    if (sizeLeft < 0 && sizeRight > 0) {
		// merge right only
        int totalSize = sizeRight + size;
        heap[index] = totalSize;
        heap[index + length + lengthRight - 1] = totalSize;
    }

    if (sizeLeft > 0 && sizeRight > 0) {
		// merge right and left
        int totalSize = sizeLeft + size + sizeRight;
        heap[index - lengthLeft] = totalSize;
        heap[index + length + lengthRight - 1] = totalSize;
    }
    
    picRestore(oldState); 
}