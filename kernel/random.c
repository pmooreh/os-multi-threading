#include "random.h"
#include "pic.h"

// multiply-with-carry pseudo-random number generator by George Marsaglia
// source: http://en.wikipedia.org/wiki/Random_number_generation

static uint32_t w;
static uint32_t z;

void randomInit(uint32_t seed) {
    w = seed;
    z = ~seed;
    if ((w == 0) || (w == 0x464fffff)) {
        w += 1;
    }
    if ((z == 0) || (z == 0x9068ffff)) {
        z += 1;
    }
}

uint32_t random() {
    int was = picDisable();
    z = 36969 * (z & 65535) + (z >> 16);
    w = 18000 * (w & 65535) + (w >> 16);
    uint32_t res = (z << 16) + w;
    picRestore(was);
    return res;
}
