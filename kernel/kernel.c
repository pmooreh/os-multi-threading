#include "stdint.h"
#include "console.h"
#include "idt.h"
#include "pic.h"
#include "pit.h"
#include "thread.h"
#include "machine.h"
#include "heap.h"
#include "random.h"

volatile int lastId = 0;

volatile int done = 0;

#define M 4

int mCount = 0;
int fCount = 0;

void f0(void* arg) {
    int* p = (int*) arg;
    int me = threadId();

    void* last[M];

    for (int i=0; i<M; i++) {
        last[i] = 0;
    }

    for (int i=0; i<*p; i++) {
        lastId = me;
        while (lastId == me) {
            int x = random() % M;
            if (last[x] != 0) {
                free(last[x]);
                getThenIncrement(&fCount,1);
            }
            size_t sz = random() % 1000;
            last[x] = malloc(sz);
             getThenIncrement(&mCount,1);
            if (last[x] == 0) {
                sayDec("failed to allocate",sz);
            }
        }
    }

    for (int i=0; i<M; i++) {
        if (last[i] != 0) {
            free(last[i]);
            getThenIncrement(&fCount,1);
        }
    }

    *p = 0;
    getThenIncrement(&done,1);
}

#define N 100

int status[N];

void one(char* name) {
    say(name);
    int count = 0;
    done = 0;

    for (int i=1; i<N; i++) {
        status[i] = i;
        int id = threadCreate(f0,(void*)&status[i]);
        if (id == -1) {
             panic("can't create thread");
        }
        count++;
    }

    sayDec("created ",count);

    while (done != count) {
        lastId = threadId();
    }

    sayDec("checking output",done);
    for (int i=1; i<N; i++) {
        if (status[i] != 0) {
            sayDec("thread failed",i);
        }
        count --;
    }

    sayDec("final count",count);
}

volatile int complete;

void h0(void* arg) {
    char* s = (char*) arg;
    say(s);
    complete = 1;
}

volatile int turn;

void g0(void* arg) {
    int me = (int)arg;

while (turn != me);
    sayDec("turn",turn);
    turn -= 1;
}

void kernelMain(void) {
    putStr("\nhello\n");

    /* Initialize heap */
    heapInit((void*)0x100000,0x100000);

    /* initialize random number generator */
    randomInit(0);

    /* initialize threads */
    threadInit();

    /* initialize idt */
    idtInit();

    /* initialize pic */
    picInit();

    /* initialize pit */
    pitInit(100000);

    /* enable interrupts */
    say("about to enable interrupts, good luck");
    picEnable();
    say("survived interrupts");

    say("creating thread");
    complete = 0;
    threadCreate(h0,"hello from thread");
	while (complete == 0);

    say("creating 10 threads");
    turn = 9;
    for (int i=0; i<10; i++) {
        threadCreate(g0,(void*)i);
    }
    while (turn != -1);
    say("10 threads done");

    say("going for the big one");
    one("---- phase1 ----");
    one("---- phase2 ----");

    if (mCount != fCount) {
        sayDec("mCount",mCount);
        sayDec("fCount",fCount);
    } else {
        say("counts match");
    }

    if (mCount < 500000) {
        sayDec("mCount is too small",mCount);
    } else {
        say("cMount >= 500000");
    }

    threadExit();

}                   
