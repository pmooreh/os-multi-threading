#include "pit.h"
#include "console.h"
#include "machine.h"
#include "idt.h"
#include "pic.h"
#include "thread.h"

#define FREQ 1193182

#define IRQ 0

void pitInit(uint32_t hz) {

     idtAdd(picBase+IRQ, (uint32_t) pitAsmHandler);

     uint32_t d = FREQ / hz;

     sayDec("pitInit freq",hz);
     
     if ((d & 0xffff) != d) {
         sayDec("pitInit invalid divider",d);
         d = 0xffff;
     }
     sayDec("pitInit divider",d);
     hz = FREQ / d;
     sayDec("initInit actual freq",hz);
     pit_do_init(d);
}

void pitHandler() {
    picEoi(IRQ);
    threadYield();
}
