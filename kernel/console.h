#ifndef _CONSOLE_H_
#define _CONSOLE_H_

#include "stdint.h"

extern void putChar(char c);
extern void putStr(char* str);
extern void putDec(int32_t val);
extern void putHex(uint32_t val);
extern void say(char* msg);
extern void sayHex(char* msg, uint32_t val);
extern void sayDec(char* msg, int32_t val);
extern void shutdown(void);
extern void panic(char* msg);

#endif
