#include "stdint.h"
#include "thread.h"
#include "console.h"
#include "pic.h"
#include "heap.h"
#include "machine.h"



// structs for threading
typedef struct Thread {
	// threads will contain the stack pointer, id, pointer to next thread in q queue, and stack
	void* stackPointer;
	int id;
	struct Thread* next;
	int stack[256];
} Thread;

typedef struct Queue {
    Thread *first;
    Thread *last;
} Queue;

// functions I use. defined at bottom of file OR in context.S
void initThread(Thread* thread, int threadId, ThreadFunc func, void* arg);
void reapThreads(int n);
void addToQueue(Queue* q, Thread* thread);
Thread* removeFirst(Queue* q);
extern void swapContext(void*, void**);
extern int getEflags();

// instance variable for our threads
static Queue ready;
static Queue reaper;
static Thread* activeThread;
int masterId;
int goodEflags;

void sayQueue() {
	say("");
	say("    Queue info");
	Thread* curr = ready.first;
	int elem = 0;

	sayHex("        activeThread address", (int)activeThread);
	//sayThread(*activeThread, activeThread->id);

	sayHex("        first's address", (int)ready.first);
	sayHex("        last's address", (int)ready.last);
	say("");

	while (curr != 0) {
		sayDec("        element", elem);
		sayDec("        thread id", (int)curr->id);
		sayHex("        thread address", (int)curr);
		sayHex("        next in queue's address", (int)curr->next);
		say("");
		elem++;
		curr = curr->next;
	}
}

void threadInit(void) {
	// get good eflags value
	goodEflags = getEflags();

	// set ready Queue
	ready.first = 0;
	ready.last = 0;

	// set reaper queue
	reaper.first = 0;
	reaper.last = 0;

	// allocate first thread, for the current one executing!
	Thread* thread = malloc(sizeof(Thread));
	int threadId = getThenIncrement(&masterId, 1);
	initThread(thread, threadId, 0, 0);

	// now set activeThread to the one just allocated!
	activeThread = thread;
}

int threadId(void) {
    return activeThread->id;
}

void threadYield(void) {

	int oldState = picDisable();

	// reap threads
	reapThreads(1);

	// get next thread in line to run
	Thread* nextThread = removeFirst(&ready);

	// any thread to run?
	if (nextThread == 0) {
		picRestore(oldState);
		return;
	}

	// add current thread to queue
	addToQueue(&ready, activeThread);

	// set activeThread to the new one that will execute
	activeThread = nextThread;

	// switch context
	// activeThread is the new thread to run, and the last in queue is the old one.
	swapContext(activeThread->stackPointer, &(ready.last->stackPointer));

	picRestore(oldState);
}

int threadCreate(ThreadFunc func, void* arg) {

	int oldState = picDisable();

	// reap threads
	//reapThreads(1);

	// allocate a new thread and initialize it
	Thread* thread = malloc(sizeof(Thread));

	// did allocation fail? if so, thread creation does too. return -1
	if (thread == 0) {
		picRestore(oldState);
		return -1;
	}

	// we're in the clear! go on initializing
	int threadId = getThenIncrement(&masterId, 1);
	initThread(thread, threadId, func, arg);

	// thread has been initialized. add to end of ready queue
	addToQueue(&ready, thread);

	picRestore(oldState);

    return threadId;
}

void threadExit(void) {

	int oldState = picDisable();

	// reap threads
	//reapThreads(1);

	// get next thread in line to run
	Thread* nextThread = removeFirst(&ready);
	Thread* old = activeThread;

	// any thread to run?
	if (nextThread == 0) {
		picRestore(oldState);
		shutdown();
	}

	// there's another thread to run

	// set activeThread to the new one that will execute
	activeThread = nextThread;

	// add this thread to the reaper queue. We want its memory back!
	addToQueue(&reaper, old);

	// switch context
	// activeThread is the new thread to run, and the last in queue is the old one.
	swapContext(activeThread->stackPointer, &(old->stackPointer));

	picRestore(oldState);
}

/* MY FUNCTIONS */

// initiates a single thread
void initThread(Thread* thread, int threadId, ThreadFunc func, void* arg) {
	// set id
	thread->id = threadId;
	// set up stack
	thread->stack[255] = (int)arg;
	thread->stack[254] = (int)threadExit;
	thread->stack[253] = (int)func;
	thread->stack[252] = 0; // %ebx
	thread->stack[251] = 0; // %esi
	thread->stack[250] = 0; // %edi
	thread->stack[249] = 0; // %ebp
	thread->stack[248] = 0; // other reg
	thread->stack[247] = 0; // other reg
	thread->stack[246] = 0; // other reg
	//thread->stack[1013] = goodEflags; // EFLAGS ??? What should these be??
	// set up stackpointer
	thread->stackPointer = &(thread->stack[246]);
	// set next to 0
	thread->next = 0;
}

// adds thread to end of q
void addToQueue(Queue* q, Thread* thread) {
	// is queue empty?
	if (q->first == 0 && q->last == 0) {
		q->first = thread;
		q->last = thread;
	}
	else {
		q->last->next = thread;
		q->last = thread;
	}

//	sayQueue();
}

// removes and returns (pointer to) first thread from q. returns 0 if no elements
Thread* removeFirst(Queue* q) {
	// is q empty?
	if (q->first == 0 && q->last == 0)
		return 0;

	// q has threads
	Thread* removed = q->first;
	q->first = removed->next;

	// was there only one element?
	if (q->first == 0)
		q->last = 0;

	removed->next = 0;
	return removed;
}

// reaps n threads from the reaper queue
void reapThreads(int n) {
	Thread* removed = removeFirst(&reaper);
	while (n > 0 && removed != 0) {

		//sayDec("freeing thread with id", removed->id);

		// free the thread
		free(removed);

		// get the next thread to reap and decrement n
		removed = removeFirst(&reaper);
		n--;
	}
}