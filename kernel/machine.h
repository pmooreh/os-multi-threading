#ifndef _MACHINE_H_
#define _MACHINE_H_

#include "stdint.h"

extern int getThenIncrement(volatile int* ptr, int d);

extern uint32_t cli(void);
extern uint32_t sti(void);

extern int inb(int port);
extern int inl(int port);
extern void outb(int port, int val);

extern void pit_do_init(uint32_t d);

extern void ltr(uint32_t tr);

extern void pageFaultHandler();

extern uint32_t getcr0();
extern uint32_t getcr3();
extern void invlpg(uint32_t);

extern void pitAsmHandler(void);

#endif
