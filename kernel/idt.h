#ifndef _IDT_H_
#define _IDT_H_

#include "stdint.h"

extern void idtInit(void);
extern void idtAdd(int index, uint32_t handler);

#endif
