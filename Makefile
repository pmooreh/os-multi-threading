default : all;

run :
	(make -C kernel kernel.img)
	-expect e0.tcl | tee results.raw
	-@egrep '^\*\*\*' results.raw > results.out 2>&1
	-@((diff -b results.out results.ok > results.diff 2>&1) && echo "--- pass") || echo "--- failed, look at results.ok, results.out, and results.diff for more information"

test :
	make clean run

	

% :
	(make -C kernel $@)

clean:
	-(make -C kernel clean)
